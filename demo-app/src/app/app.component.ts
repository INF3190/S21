import { LOCALE_ID, Inject, Component } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'demo-app';
  toLocale = "en";
  languageHref: string = "";
  minutes: number = 2;
  constructor(
    @Inject(LOCALE_ID) public locale: string,
    private currentRoute: Router
  ) {
    this.ngOnInit();
  }

  ngOnInit() {
    this.languageHref = this.currentRoute.url;

    this.toogle();
  }

  toogle(){
    if (this.locale == "en") {
      this.toLocale = "fr";
    }
    else {
      this.toLocale = "en";
    }
  }
}
